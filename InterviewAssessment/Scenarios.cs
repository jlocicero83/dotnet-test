﻿using System.Collections.Generic;
using System.Linq;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario1(string str)
        {
            //ex. using linq
            //var result = new string(str.ToCharArray().Reverse().ToArray());
            //return result.ToString();
            if (string.IsNullOrEmpty(str)) return "";
            string result = "";
            for(int i = str.Length - 1; i >= 0; i--)
            {
                result += str[i];
            }
            return result;
        }

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            //possible edge cases handling zeros
            int n = @base;

            for (int i = 1; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node)
        {
            if (node == null) return "";

            //build a list of visited nodes
            List<string> preorderTraversalResult = new List<string>();

            //keep track of visited nodes by pushing/popping with a stack
            Stack<Node> stack = new Stack<Node>();
           
            stack.Push(node);

            while(stack.Count != 0)
            {
                Node current = stack.Pop();


                if (current != null)
                {
                    preorderTraversalResult.Add(current.Text);

                    //try to cast as a tree/root - if unsuccessful, it's a leaf node.
                    try
                    {
                        Tree currNodeAsTree = (Tree)current;
                        if (currNodeAsTree.Children.Count() > 0)
                        {
                            for (int i = currNodeAsTree.Children.Count() - 1; i >= 0; i--)
                            {
                                //try to cast the child as a tree
                                try
                                {
                                 stack.Push((Tree)currNodeAsTree.Children.ElementAt(i));

                                }
                                //otherwise push it as a node
                                catch
                                {
                                    stack.Push(currNodeAsTree.Children.ElementAt(i));
                                }
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                
                }
            }

            return string.Join("-", preorderTraversalResult);

        }
    }

    public class Node
    {
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
